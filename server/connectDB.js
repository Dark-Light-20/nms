/**
 * Connection processes.
 * @module server:connectDB
 */

// Modules
const mongoose = require('mongoose');
// DB Credentials
const DBURI = `mongodb://${encodeURIComponent(
  process.env.DB_USER
)}:${encodeURIComponent(process.env.DB_PSWD)}@${encodeURIComponent(
  process.env.DB_HOST
)}:${encodeURIComponent(process.env.DB_PORT)}/${encodeURIComponent(
  process.env.DB_NAME
)}`;

module.exports = async () => {
  try {
    await mongoose.connect(DBURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    mongoose.set('useFindAndModify', false);
  } catch (err) {
    console.error('DB Connection error: ', err);
  }
};
