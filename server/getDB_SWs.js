/**
 * Retrieve database Switches list.
 * @module server:getDB_SWs
 */

// DB Models
const SW_List = require('../models/SW_List');

/**
 * Load database Switches list into the NMS memory.
 * @returns {SWs} Object with the Switches list
 */
function getDB_SWs() {
  return new Promise(async (resolve, reject) => {
    try {
      const SWs = {};
      const SWsQuery = await SW_List.find({});
      for (const sw of SWsQuery)
        SWs[sw.IP] = {
          IP: sw.IP,
          name: sw.name,
          parent: sw.parent,
          parentPort: sw.parent_port,
          mail: sw.mail,
          notified: false
        };
      resolve(SWs);
    } catch (err) {
      reject(err);
    }
  });
}

module.exports = getDB_SWs;
