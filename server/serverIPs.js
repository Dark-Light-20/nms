/**
 * Retrieve server interfaces addresses.
 * @module server:serverIPs
 */

// Modules
const { networkInterfaces } = require('os');

/**
 * Get server IPV4 interfaces addresses.
 * @returns {[String]} Server IPV4 IPs list
 */
function getServerIPs() {
  const netints = networkInterfaces();
  const IPs = [];
  for (const name of Object.keys(netints))
    for (const net of netints[name])
      if (net.family === 'IPv4' && !net.internal) IPs.push(net.address);
  return IPs;
}

module.exports = getServerIPs;
