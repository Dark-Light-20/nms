/**
 * Send mail notification about device status change.
 * @module server:sendMail
 */

// Modules
const nodemailer = require('nodemailer');

/**
 * Create mail connection and attach it to the server.
 * @param {Express} APP Server application
 */
function createTransport(APP) {
  APP.set(
    'mailTransport',
    nodemailer.createTransport({
      host: process.env.MAILHOST,
      port: process.env.MAILPORT,
      secure: false,
      pool: true,
      auth: {
        user: process.env.USERMAIL,
        pass: process.env.PSWDMAIL
      }
    })
  );
}

/**
 * Send a mail notification about device status change.
 * @param {nodemailer.Transport} transporter Mail transporter
 * @param {String} content Body content for mail
 */
function sendMail(transporter, content) {
  transporter.sendMail(
    {
      from: `"NMS-Udenar" <${process.env.USERMAIL}>`,
      to: `${process.env.RECEIVERMAIL}`,
      subject: content.subject,
      text: content.text
    },
    (err, info) => {
      if (err) console.error('Mail Error!!! : ', err);
      else console.log('Mail sent : ', info);
    }
  );
}

module.exports.createTransport = createTransport;
module.exports.sendMail = sendMail;
