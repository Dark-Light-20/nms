/**
 * Devices monitor daemon and logic.
 * @module server:monitor
 */

// Modules
const ping = require('ping');
const moment = require('moment');
const mongoose = require('mongoose');
// DB Models
const Status_Report = require('../models/Status_Report');
// Mail sender
const sendMail = require('../server/sendMail').sendMail;

/**
 * Run monitor daemon for the NMS devices list.
 * @param {nodemailer.Transport} mailTransport Mail transporter
 * @param {SWs} SWs Object with the Switches list
 */
async function monitor(mailTransport, SWs) {
  const IPs = Object.keys(SWs);
  for (const sw of IPs) {
    const currentSW = sw; // Reference current Switch (patch for async processes)
    setTimeout(async () => {
      try {
        // First request
        const result = await ping.promise.probe(currentSW, {
          extra: ['-c', `${process.env.MONITOR_COUNT}`]
        });
        if (!result.alive) {
          if (SWs[currentSW] && !SWs[currentSW].notified) {
            // Second request to verify if device is down
            const verify = await ping.promise.probe(currentSW, {
              extra: ['-c', `${process.env.MONITOR_VERIFY}`]
            });
            if (!verify.alive) {
              const date = moment()
                .locale('es')
                .format('dddd DD MMMM YYYY, hh:mm:ss A');
              if (SWs[currentSW].mail) {
                const msg = {
                  subject: `NMS-Udenar. Alerta! Desconexión de dispositivo: ${currentSW}`,
                  text: `El sistema de monitoreo reporta que se ha perdido la conexión al dispositivo ${currentSW}, (${SWs[currentSW].name}). Fecha de reporte: ${date}`
                };
                sendMail(mailTransport, msg);
              }
              new Status_Report({
                _id: new mongoose.Types.ObjectId(),
                sw_IP: currentSW,
                sw_name: SWs[currentSW].name,
                action: 'down',
                date: date
              }).save();
              console.error(date, `=> Switch: ${sw} está inactivo...`);
              SWs[currentSW].notified = true;
            } else result.alive = true;
          }
        }
        // If down device awakes reset the notification flag
        else if (SWs[currentSW] && SWs[currentSW].notified) {
          const date = moment()
            .locale('es')
            .format('dddd DD MMMM YYYY, hh:mm:ss A');
          SWs[currentSW].notified = false;
          if (SWs[currentSW].mail) {
            const msg = {
              subject: `NMS-Udenar. Notificación! Recuperación de dispositivo: ${currentSW}`,
              text: `El sistema de monitoreo reporta que se ha restaurado la conexión al dispositivo ${currentSW}, (${SWs[currentSW].name}). Fecha de reporte: ${date}`
            };
            sendMail(mailTransport, msg);
          }
          new Status_Report({
            _id: new mongoose.Types.ObjectId(),
            sw_IP: currentSW,
            sw_name: SWs[currentSW].name,
            action: 'up',
            date: date
          }).save();
          console.log(date, `=> Switch: ${currentSW} está activo de nuevo...`);
        }
        // Assign ping response values to device
        if (SWs[currentSW]) SWs[currentSW].alive = result.alive;
        if (SWs[currentSW]) SWs[currentSW].ms = result.avg;
      } catch (err) {
        console.error(err);
      }
    }, 500);
  }
}

module.exports = monitor;
