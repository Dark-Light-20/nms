/**
 * Backup daemon and logic.
 * @module server:backups
 */

// Modules
const schedule = require('node-schedule');
const mongoose = require('mongoose');
const moment = require('moment');
// DB Models
const SW_List = require('../models/SW_List');
const Backup_Report = require('../models/Backup_Report');
// System Controller functions
const getSwData = require('../controllers/swController').getSwData;

/**
 * Run backup daemon for the NMS.
 * @param {Express} APP Server application 
 */
function backupsDaemon(APP) {
  schedule.scheduleJob('backups', '0 0 1 */3 *', async () => {
    const SWs = Object.values(APP.get('SWs'));
    const serverIPs = APP.get('serverIPs');
    for (sw of SWs) {
      // NMS daemon direct exceptions
      if (sw.name === 'FIREWALL' || serverIPs.find((IP) => IP === sw.IP))
        continue;
      try {
        const swData = await getSwData(sw.IP, APP);
        new Backup_Report({
          _id: new mongoose.Types.ObjectId(),
          sw_IP: sw.IP,
          type: 'historic',
          db_data: await SW_List.findOne({ IP: sw.IP }, { IP: 0, _id: 0 }),
          basic: swData.basic,
          VLANs: swData.VLANs,
          ports: swData.ports,
          date: moment().locale('es').format('dddd DD MMMM YYYY, hh:mm:ss A')
        }).save();
        console.log('Backup saved: ', sw.IP);
      } catch (err) {
        console.error('Backup Error!!!: ', err);
      }
    }
  });
}

module.exports = backupsDaemon;
