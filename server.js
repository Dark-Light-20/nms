// Imports
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const session = require('express-session');
require('dotenv').config({ path: path.join(__dirname, '.env') });

// App
const app = express();

// Open DB connection
const dbConnection = require('./server/connectDB');
dbConnection();

// Open mail connection
const mailConnection = require('./server/sendMail').createTransport;
mailConnection(app);

// Load utilities
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(
  session({
    secret: process.env.SESSION_PSWD,
    resave: false,
    saveUninitialized: false
  })
);

// Views engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Get Server IPs
const getServerIPs = require('./server/serverIPs');
app.set('serverIPs', getServerIPs());

// Load Switches
const getDB_SWs = require('./server/getDB_SWs');
const monitorD = require('./server/monitor');
const backupsDaemon = require('./server/backups');
async function loadSWs() {
  try {
    // Retrieve Switches from database
    app.set('SWs', await getDB_SWs());
    // Switches monitor daemon
    setInterval(
      () => monitorD(app.get('mailTransport'), app.get('SWs')),
      process.env.MONITOR_INTERVAL
    );
    // Switches backups daemon
    backupsDaemon(app);
  } catch (err) {
    console.error(err);
  }
}
loadSWs();

// Import Routes
const statusRouter = require('./routes/status');
const reportsRouter = require('./routes/reports');
const appBackupRouter = require('./routes/appBackup');
const appRouter = require('./routes/app');
const userRouter = require('./routes/user');
//const tryRouter = require('./routes/try'); // For dev

// Use Routes
app.use('/status', statusRouter);
app.use('/reports', reportsRouter);
app.use('/appBackup', appBackupRouter);
app.use('/', appRouter);
app.use('/', userRouter);
//app.use('/try', tryRouter); // For dev

// Start application
app.listen(process.env.APP_PORT, () => {
  console.log('Running NMS on port:', process.env.APP_PORT);
});
