/**
 * This model represent a Switch model reference by the SNMP device OID.
 * @module models:SW_Model
 */

const mongoose = require('mongoose');

const SW_Model_Schema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    oid: {
      type: String,
      required: true
    },
    brand: {
      type: String,
      required: true
    },
    model: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: true
    }
  },
  {
    versionKey: false
  }
);

module.exports = mongoose.model('SWModels', SW_Model_Schema, 'SWModels');
