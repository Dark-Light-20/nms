/**
 * This model represent the Switch information for the system. This data is given by the users.
 * the NMS required struct.
 * @module models:SW_List
 */

const mongoose = require('mongoose');

const SW_List_Schema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    IP: {
      type: String,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    parent: {
      type: String
    },
    parent_port: {
      type: String
    },
    campus: {
      type: String,
      required: true
    },
    zone: {
      type: String,
      required: true
    },
    mail: {
      type: Boolean,
      required: true
    }
  },
  {
    versionKey: false
  }
);

module.exports = mongoose.model('SWList', SW_List_Schema, 'Switches');
