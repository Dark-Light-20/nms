/**
 * This model represent a system modification report. This has all NMS system information in Switch List. 
 * @module models:Modif_Report
 */

const mongoose = require('mongoose');

const Mod_Reports_Schema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    user: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: true
    },
    date: {
      type: String,
      required: true
    },
    sw_IP: {
      type: String,
      required: true
    },
    data: {
      name: {
        type: String,
        required: true
      },
      parent: {
        type: String,
        required: true
      },
      campus: {
        type: String,
        required: true
      },
      zone: {
        type: String,
        required: true
      },
      mail: {
        type: Boolean,
        required: true
      }
    }
  },
  {
    versionKey: false
  }
);

module.exports = mongoose.model(
  'ModReport',
  Mod_Reports_Schema,
  'ModificationReports'
);
