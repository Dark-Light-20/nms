/**
 * This model represent a Switch status change report.
 * @module models:Status_Report
 */

const mongoose = require('mongoose');

const Status_Report_Schema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    sw_IP: {
      type: String,
      required: true
    },
    sw_name: {
      type: String,
      required: true
    },
    action: {
      type: String,
      required: true
    },
    date: {
      type: String,
      required: true
    }
  },
  {
    versionKey: false
  }
);

module.exports = mongoose.model(
  'StatusReports',
  Status_Report_Schema,
  'StatusReports'
);
