/**
 * This model represent a NMS user for authentication.
 * @module models:Users
 */

const mongoose = require('mongoose');

const User_Schema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    user: {
      type: String,
      required: true
    },
    pswd: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: true
    }
  },
  {
    versionKey: false
  }
);

module.exports = mongoose.model('Users', User_Schema, 'Users');
