/**
 * This model represent a Switch configuration backup. Uses the getSwData method from swController to conserve
 * the NMS required struct.
 * @module models:Backup_Report
 */

const mongoose = require('mongoose');

const Backup_Report_Schema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    sw_IP: {
      type: String,
      required: true
    },
    type: {
      type: String,
      required: true
    },
    db_data: {
      name: {
        type: String,
        required: true
      },
      parent: {
        type: String,
        required: true
      },
      parent_port: {
        type: String
      },
      campus: {
        type: String,
        required: true
      },
      zone: {
        type: String,
        required: true
      },
      mail: {
        type: Boolean,
        required: true
      }
    },
    basic: {
      oid: {
        type: String,
        required: true
      },
      uptime: {
        type: String,
        required: true
      },
      contact: {
        type: String
      },
      name: {
        type: String,
        required: true
      },
      location: {
        type: String
      },
      serial: {
        type: String
      },
      brand: {
        type: String
      },
      model: {
        type: String
      },
      type: {
        type: String
      }
    },
    VLANs: {
      type: Object,
      required: true
    },
    ports: {
      type: Object,
      required: true
    },
    date: {
      type: String,
      required: true
    }
  },
  {
    versionKey: false
  }
);

module.exports = mongoose.model(
  'BackupReports',
  Backup_Report_Schema,
  'BackupReports'
);
