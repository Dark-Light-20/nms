/**
 * Routes for application interaction and use.
 * @module routes:app
 */

// Express Router
const router = require('express').Router();
// Controllers
const swListController = require('../controllers/swListController');
const swController = require('../controllers/swController');
const doBK = require('../server/do-bk');
// Authentication middleware
const isAuthenticated = require('../controllers/userController')
  .isAuthenticated;
const notVisorUser = require('../controllers/userController').notVisorUser;
// Version
const version = require('../package.json').version;
// File middleware
const multer = require('multer');
const upload = multer();

router.get('/', isAuthenticated, (req, res) =>
  res.render('index', {
    title: 'NMS-UDENAR',
    navbarText: 'NMS-UDENAR',
    user: { name: req.session.user, type: req.session.type }
  })
);
router.get('/map', isAuthenticated, (req, res) =>
  res.render('map', {
    title: 'Network-Map',
    navbarText: 'Mapa de Red',
    user: { name: req.session.user, type: req.session.type }
  })
);
router.get('/switchList', isAuthenticated, swListController.getList);
router
  .route('/switch/:IP')
  .all(isAuthenticated)
  .get(swController.getSwitch)
  // Next routes denied for "visor" user
  .post(notVisorUser, upload.none(), swController.addSwitch)
  .patch(notVisorUser, upload.none(), swController.editSwitch)
  .delete(notVisorUser, swController.deleteSwitch);
router.get('/about', isAuthenticated, (req, res) =>
  res.render('about', {
    title: 'About',
    navbarText: 'Acerca de',
    version: version,
    user: { name: req.session.user, type: req.session.type }
  })
);
router.get('/doBK', isAuthenticated, doBK);

module.exports = router;
