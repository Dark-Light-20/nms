/**
 * Routes for system reports.
 * @module routes:reports
 */

// Express Router
const router = require('express').Router();
// Controllers
const reportController = require('../controllers/reportController');
// Authentication middleware
const isAuthenticated = require('../controllers/userController')
  .isAuthenticated;
const notVisorUser = require('../controllers/userController').notVisorUser;

router.get('/', isAuthenticated, (req, res) =>
  res.render('reports', {
    title: 'Reports',
    navbarText: 'Reportes',
    user: { name: req.session.user, type: req.session.type }
  })
);
// "modifications" route denied for "visor" user
router.get(
  '/modifications',
  isAuthenticated,
  notVisorUser,
  reportController.getModifications
);
router.get(
  '/modifications/:id',
  isAuthenticated,
  notVisorUser,
  reportController.getModificationData
);
router.get('/statuses', isAuthenticated, reportController.getStatuses);
router.get('/backups', isAuthenticated, reportController.getBackups);
router.get('/backups/:IP', isAuthenticated, reportController.getBackup);

module.exports = router;
