/**
 * Routes for API layer of the system.
 * @module routes:status
 */

// Express Router
const router = require('express').Router();
// Controllers
const monitorController = require('../controllers/monitorController');
const swController = require('../controllers/swController');
const swListController = require('../controllers/swListController');
const trafficController = require('../controllers/trafficController');
// Authentication middleware
const isAuthenticated = require('../controllers/userController')
  .isAuthenticated;

router.get('/monitor', isAuthenticated, monitorController.getSWs);
router.get('/switch/:IP', isAuthenticated, swController.getSwDataAPI);
router.get('/traffic/:IP', isAuthenticated, trafficController.getSwUsage);
router.get('/swList', isAuthenticated, swListController.getListAPI);

module.exports = router;
