/**
 * Routes for application DB backup.
 * @module routes:appBackup
 */

// Express Router
const router = require('express').Router();
// Controllers
const appBackupController = require('../controllers/appBackupController');
// Authentication middleware
const isAuthenticated = require('../controllers/userController')
  .isAuthenticated;
const isAdminUser = require('../controllers/userController').isAdminUser;
// File middleware
const multer = require('multer');
const upload = multer();

router.get('/', isAuthenticated, isAdminUser, (req, res) => {
  res.render('appBackup', {
    title: 'Backup NMS',
    navbarText: 'Copia de seguridad NMS',
    user: { name: req.session.user, type: req.session.type }
  })
})
router.get('/download', isAuthenticated, isAdminUser, appBackupController.getAppBackup);
router.post('/', isAuthenticated, isAdminUser, upload.single('NMSbackup'),
  appBackupController.loadAppBackup
);

module.exports = router;
