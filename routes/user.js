/**
 * Routes for user management and system sessions authentication.
 */

// Express Router
const router = require('express').Router();
// Controllers
const userController = require('../controllers/userController');
// File middleware
const multer = require('multer');
const upload = multer();
router.use(upload.none());

router
  .route('/login')
  .get((req, res) => res.render('login', { title: 'Login' }))
  .post(userController.getUser);
router
  .route('/register')
  .all(userController.isAuthenticated)
  .all(userController.isAdminUser)
  .get((req, res) =>
    res.render('signup', {
      title: 'Register User',
      navbarText: 'Registrar usuario',
      user: { name: req.session.user, type: req.session.type }
    })
  )
  .post(userController.addUser);
router
  .route('/user')
  .all(userController.isAuthenticated)
  .get((req, res) =>
    res.render('modifyUser', {
      title: 'Modify Password',
      navbarText: 'Modificar contraseña',
      user: { name: req.session.user, type: req.session.type }
    })
  )
  .post(userController.modifyUser);
router
  .route('/deleteUser')
  .all(userController.isAuthenticated)
  .all(userController.isAdminUser)
  .get(async (req, res) =>
    res.render('deleteUser', {
      title: 'Delete User',
      navbarText: 'Eliminar usuario',
      user: { name: req.session.user, type: req.session.type },
      users: await userController.getUsers()
    })
  )
  .post(userController.deleteUser);
router.get('/logout', userController.isAuthenticated, userController.logOut);

module.exports = router;
