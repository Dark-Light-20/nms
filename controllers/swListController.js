/**
 * Controller for retrieve devices list
 * @module controllers:swListController
 */

// DB Models
const SW_List = require('../models/SW_List');

/**
 * Retrieve devices list from database and render the view to client.
 * @param {Request} req
 * @param {Response} res
 */
async function getList(req, res) {
  try {
    res.render('swList', {
      title: 'Switches list',
      navbarText: 'Listado de Switches',
      swList: await SW_List.find(),
      serverIPs: req.app.get('serverIPs'),
      user: { name: req.session.user, type: req.session.type }
    });
  } catch (err) {
    res.status(500).render('error', {
      title: 'Switches list',
      navbarText: 'Listado de Switches',
      msg:
        'No se pudo obtener el listado de switches, intente de nuevo en unos segundos.',
      error: err,
      user: { name: req.session.user, type: req.session.type }
    });
  }
}

/**
 * Retrieve devices list from database and send data them to client.
 * @param {Request} req
 * @param {Response} res
 */
async function getListAPI(req, res) {
  try {
    res.json(await SW_List.find({}));
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
}

module.exports.getList = getList;
module.exports.getListAPI = getListAPI;
