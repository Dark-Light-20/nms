/**
 * Controller for user management and authentication processes.
 * @module controllers:userController
 */

// Modules
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
// DB Models
const User = require('../models/User');

/**
 * Verify if given user exists in databse and its credentials are correct, in case they are, create a client session and returns user's data.
 * @param {Request} req
 * @param {Response} res
 */
async function getUser(req, res) {
  const { user, pswd } = req.body;
  if (!user || !pswd) return res.status(401).send('Campos vacios');
  try {
    const bdUser = await User.findOne({ user: user });
    if (!bdUser) return res.status(401).send('Credenciales incorrectas...');
    const match = await bcrypt.compare(pswd, bdUser.pswd);
    if (match) {
      req.session.user = bdUser.user;
      req.session.type = bdUser.type;
      res.send(req.session.user);
    } else res.status(401).send('Credenciales incorrectas...');
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
}

/**
 * Add user to database.
 * @param {Request} req
 * @param {Response} res
 */
async function addUser(req, res) {
  const { user, type, pswd1, pswd2 } = req.body;
  if (!user || !type || !pswd1 || !pswd2)
    return res.status(401).send('Campos vacios');
  if (pswd1 !== pswd2) return res.status(401).send('Contraseñas no coinciden');
  try {
    if (await User.findOne({ user: user }))
      return res.status(409).send('El usuario ya existe');
    const hash = await bcrypt.hash(pswd1, 10);
    const newUser = new User({
      _id: new mongoose.Types.ObjectId(),
      user: user,
      pswd: hash,
      type: type
    });
    await newUser.save();
    res.send('Usuario registrado');
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
}

/**
 * Change user password.
 * @param {Request} req
 * @param {Response} res
 */
async function modifyUser(req, res) {
  const { pswd, pswd1, pswd2 } = req.body;
  if (!pswd || !pswd1 || !pswd2) return res.status(401).send('Campos vacios');
  if (pswd1 !== pswd2) return res.status(401).send('Contraseñas no coinciden');
  try {
    const bdUser = await User.findOne({ user: req.session.user });
    const match = await bcrypt.compare(pswd, bdUser.pswd);
    if (match) {
      const hash = await bcrypt.hash(pswd1, 10);
      bdUser.pswd = hash;
      await bdUser.save();
      res.send('UPDATED');
    } else res.status(401).send('Credenciales incorrectas...');
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
}

/**
 * Retrieve user list from database.
 * @returns {[String]} username list
 */
async function getUsers() {
  try {
    const users = [];
    const dbUsers = await User.find({});
    for (const user of dbUsers) users.push(user.user);
    return users;
  } catch (err) {
    console.error(err);
    return err;
  }
}

/**
 * Delete given user from database.
 * @param {Request} req
 * @param {Response} res
 */
async function deleteUser(req, res) {
  const user = req.body.user;
  if (!user) return res.status(401).send('Campos vacios');
  try {
    await User.deleteOne({ user: user }).exec();
    res.send('DELETED');
  } catch (err) {
    console.error(err);
    return err;
  }
}

/**
 * Middleware for verify if a request is authenticated to procced to next process.
 * @param {Request} req
 * @param {Response} res
 * @param {Express.middleware} next
 */
function isAuthenticated(req, res, next) {
  req.session.user ? next() : res.redirect('/login');
}

/**
 * Middleware for verify if user isn't "visor" type.
 * @param {Request} req
 * @param {Response} res
 * @param {Express.middleware} next
 */
function notVisorUser(req, res, next) {
  req.session.type !== '3' ? next() : res.redirect('/');
}

/**
 * Middleware for verify if user is admin type.
 * @param {Request} req
 * @param {Response} res
 * @param {Express.middleware} next
 */
function isAdminUser(req, res, next) {
  req.session.type === '1' ? next() : res.redirect('/');
}

/**
 * End client session.
 * @param {Request} req
 * @param {Response} res
 */
function logOut(req, res) {
  if (req.session.user) {
    delete req.session.user;
    delete req.session.type;
    res.redirect('/login');
  }
}

module.exports.getUser = getUser;
module.exports.addUser = addUser;
module.exports.modifyUser = modifyUser;
module.exports.deleteUser = deleteUser;
module.exports.getUsers = getUsers;
module.exports.isAuthenticated = isAuthenticated;
module.exports.notVisorUser = notVisorUser;
module.exports.isAdminUser = isAdminUser;
module.exports.logOut = logOut;
