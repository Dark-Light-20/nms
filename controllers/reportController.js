/**
 * Controller for system reports management
 * @module controllers:reportController
 */

// DB Models
const Modif_Report = require('../models/Modif_Report');
const Status_Report = require('../models/Status_Report');
const Backup_Report = require('../models/Backup_Report');

/**
 * Retrieve system modifications report list from database and render the view to client.
 * @param {Request} req
 * @param {Response} res
 */
async function getModifications(req, res) {
  try {
    res.render('modifications', {
      title: 'Modifications',
      navbarText: 'Listado de Modificaciones',
      modList: (await Modif_Report.find({}, { data: 0, sw_Data: 0 })).reverse(),
      user: { name: req.session.user, type: req.session.type }
    });
  } catch (err) {
    res.status(500).render('error', {
      title: 'Modifications',
      navbarText: 'Listado de Modificaciones',
      msg:
        'No se pudo obtener el listado de modificaciones, intente de nuevo en unos segundos.',
      error: err,
      user: { name: req.session.user, type: req.session.type }
    });
  }
}

/**
 * Retrieve data of a modification report from database and send it to client.
 * @param {Request} req
 * @param {Response} res
 */
async function getModificationData(req, res) {
  try {
    const data = await Modif_Report.findById(req.params.id, 'data');
    res.json(data);
  } catch (err) {
    res.status(500).send(err);
  }
}

/**
 * Retrieve devices status change reports from database and render the view to client.
 * @param {Request} req
 * @param {Response} res
 */
async function getStatuses(req, res) {
  try {
    res.render('statuses', {
      title: 'Statuses',
      navbarText: 'Histórico de caidas',
      statusList: (await Status_Report.find({})).reverse(),
      user: { name: req.session.user, type: req.session.type }
    });
  } catch (err) {
    res.status(500).render('error', {
      title: 'Modifications',
      navbarText: 'Historico de caidas',
      msg:
        'No se pudo obtener el histórico de caidas, intente de nuevo en unos segundos.',
      error: err,
      user: { name: req.session.user, type: req.session.type }
    });
  }
}

/**
 * Retrieve devices list from database that have backups and render the view to client.
 * @param {Request} req
 * @param {Response} res
 */
async function getBackups(req, res) {
  try {
    res.render('backupsList', {
      title: 'Backups',
      navbarText: 'Copias de seguridad',
      swList: await Backup_Report.find({}, { sw_IP: 1, _id: 0 }).distinct(
        'sw_IP'
      ),
      user: { name: req.session.user, type: req.session.type }
    });
  } catch (err) {
    res.status(500).render('error', {
      title: 'Backups',
      navbarText: 'Copias de seguridad',
      msg:
        'No se pudo obtener el listado de copias de seguridad, intente de nuevo en unos segundos.',
      error: err,
      user: { name: req.session.user, type: req.session.type }
    });
  }
}

/**
 * Retrieve backups from a specific device from database and render the view to client.
 * @param {Request} req
 * @param {Response} res
 */
async function getBackup(req, res) {
  const IP = req.params.IP;
  try {
    res.render('backup', {
      title: `Backups: ${IP}`,
      navbarText: `Copias de switch: ${IP}`,
      backups: (
        await Backup_Report.find({ sw_IP: IP, type: 'historic' })
      ).reverse(),
      current: await Backup_Report.findOne({ sw_IP: IP, type: 'current' }),
      user: { name: req.session.user, type: req.session.type }
    });
  } catch (err) {
    res.status(500).render('error', {
      title: 'Backups',
      navbarText: 'Copias de seguridad',
      msg:
        'No se pudo obtener el listado de copias de seguridad, intente de nuevo en unos segundos.',
      error: err,
      user: { name: req.session.user, type: req.session.type }
    });
  }
}

module.exports.getModifications = getModifications;
module.exports.getModificationData = getModificationData;
module.exports.getStatuses = getStatuses;
module.exports.getBackups = getBackups;
module.exports.getBackup = getBackup;
