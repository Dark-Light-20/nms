/**
 * Controller for device traffic data management
 * @module controllers:trafficController
 */

// Modules
const snmp = require('net-snmp');
// net-snmp Session Data
const CMSTRING = process.env.CMSTRING;
const options = {
  version: snmp.Version2c
};
// Controller requieres
const getSwOID = require('./swController').getSwOID;
const getSwPorts = require('./swController').getSwPorts;

/**
 * Return ports current traffic from a switch and send the percentage values to client.
 * The traffic values represent usage in a time interval of approximately 5 seconds.
 * @param {Request} req
 * @param {Response} res
 */
async function getSwUsage(req, res) {
  // Get device IP address (url ex: http://10.16.17.254:8000/status/traffic/10.16.16.60)
  const IP = req.params.IP;
  // Start SNPM session to device (IP, community string, options)
  const session = snmp.createSession(IP, CMSTRING, options);
  let response = [];

  try {
    const SW_OID = await getSwOID(session);
    let PORTS = await getSwPorts(session, SW_OID);
    // Get first traffic measure
    let req1 = await getTraffic(session, PORTS);
    let req2;
    await new Promise((resolve, reject) => {
      setTimeout(async () => {
        try {
          // Get second traffic measure after approximately 5 seconds
          req2 = await getTraffic(session, PORTS);
          PORTS = Object.keys(req1);
          for (const port of PORTS) {
            let diffIn;
            let diffOut;
            // Validation if counter restarts
            if (req2[port].in < req1[port].in) {
              diffIn = 4294967296 - req1[port].in + req2[port].in;
              diffOut = 4294967296 - req1[port].out + req2[port].out;
            } else {
              diffIn = req2[port].in - req1[port].in;
              diffOut = req2[port].out - req1[port].out;
            }
            // Calculate in/out usage in percentage according to port speed
            const usageIn =
              ((diffIn * 8 * 100) / (5 * req1[port].speed * 1000000)) * 100;
            const usageOut =
              ((diffOut * 8 * 100) / (5 * req1[port].speed * 1000000)) * 100;
            response.push({
              id: port,
              usageIn: usageIn.toFixed(2),
              usageOut: usageOut.toFixed(2)
            });
          }
          resolve(response);
        } catch (err) {
          reject(err);
        }
      }, 4500);
    });
  } catch (err) {
    response.error = err.toString();
    console.error(err.toString());
  }

  session.close();
  res.json(response);
}

/**
 * Get octects counter values in ports from a switch.
 * @param {snmp.Session} session
 * @param {[PORT]} PORTS Switch ports list to which retrieve octects values
 * @returns {[PORT]} Object that contains each Switch active port with its in/out octects and speed
 */
function getTraffic(session, PORTS) {
  let portsList = [];
  // request octects values only for active ports.
  for (const port in PORTS) if (PORTS[port].status === 1) portsList.push(port);
  return new Promise(async (resolve) => {
    const result = {};
    for (const port of portsList) {
      result[port] = {};
      // OIDs for traffic on interface in/out (in octects) and port-speed
      const OIDs = [
        '1.3.6.1.2.1.2.2.1.10.' + port,
        '1.3.6.1.2.1.2.2.1.16.' + port,
        '1.3.6.1.2.1.31.1.1.1.15.' + port
      ];
      let values = [];
      try {
        await new Promise((resolve, reject) => {
          session.get(OIDs, (error, results) => {
            if (error) reject(error);
            else {
              for (const item of results) {
                if (snmp.isVarbindError(item))
                  console.error(snmp.varbindError(item));
                else values.push(item.value);
              }
              resolve();
            }
          });
        });
        result[port].in = values[0];
        result[port].out = values[1];
        result[port].speed = values[2];
      } catch (err) {
        result[port] = err;
      }
    }
    resolve(result);
  });
}

module.exports.getSwUsage = getSwUsage;
module.exports.getTraffic = getTraffic;
