/**
 * Controller for Switch management, request to SNMP protocol its data and procces them.
 * @module controllers:swController
 */

// Modules
const snmp = require('net-snmp');
const prettyms = require('pretty-ms');
const mongoose = require('mongoose');
const moment = require('moment');
// DB Models
const SW_Model = require('../models/SW_Model');
const SW_List = require('../models/SW_List');
const Modif_Report = require('../models/Modif_Report');
const Backup_Report = require('../models/Backup_Report');
// net-snmp Session Data
const CMSTRING = process.env.CMSTRING;
const options = {
  version: snmp.Version2c
};

/**
 * Get from given IP the Switch data (name, basic info, VLANs list and Ports data (name, alias, speed, type and VLANs assigned)) and render view to client.
 * @param {Request} req
 * @param {Response} res
 */
async function getSwitch(req, res) {
  // Get Switch data from SNMP request
  let swData;
  try {
    swData = await getSwData(req.params.IP, req.app);
  } catch (swDataError) {
    console.error('get Switch Error: ', swDataError);
  }
  // Get switch name from database
  let swName;
  try {
    swName = await getSwDbName(req.params.IP);
    if (swName) swName = swName.name;
    else swName = req.params.IP;
  } catch (err) {
    console.error(`${err.section} Error: `, err);
    swName = req.params.IP;
  }
  // Render No-ping/No-SNMP Error view
  if (!swData) {
    res.status(500).render('error', {
      title: 'Switch Error',
      navbarText: `Switch: ${swName}`,
      IP: req.params.IP,
      msg: `No se pudo obtener información del switch: ${req.params.IP}, posiblemente el switch no tiene implementado y configurado el protocolo SNMP o no hay conexión al switch.`,
      user: { name: req.session.user, type: req.session.type }
    });
  } else {
    res.render('switch', {
      title: 'Switch info',
      navbarText: `Switch: ${swName}`,
      IP: req.params.IP,
      swData: swData,
      user: { name: req.session.user, type: req.session.type }
    });
  }
}

/**
 * Add new Switch to NMS and database.
 * @param {Request} req
 * @param {Response} res
 */
async function addSwitch(req, res) {
  const { IP, name, parent, campus, zone, mail } = req.body;
  if (!IP || !name || !parent || !campus || !zone)
    return res.status(401).send('Campos vacios');
  const newSwitch = new SW_List({
    _id: new mongoose.Types.ObjectId(),
    IP: IP,
    name: name,
    parent: parent,
    campus: campus,
    zone: zone,
    mail: mail ? true : false
  });
  const newReport = new Modif_Report({
    _id: new mongoose.Types.ObjectId(),
    user: req.session.user,
    type: 'Addition',
    date: moment().locale('es').format('dddd DD MMMM YYYY, hh:mm:ss A'),
    sw_IP: IP,
    data: {
      name: name,
      parent: parent,
      campus: campus,
      zone: zone,
      mail: mail ? true : false
    }
  });
  try {
    await newSwitch.save();
    await newReport.save();
    // add Switch to NMS devices list for daemons
    req.app.get('SWs')[IP] = {
      IP: IP,
      name: name,
      parent: parent,
      mail: mail ? true : false,
      notified: false
    };
    getSwData(IP, req.app);
    res.send('ADD');
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
}

/**
 * Change Switch info to database.
 * @param {Request} req
 * @param {Response} res
 */
async function editSwitch(req, res) {
  const oldIP = req.params.IP;
  const { IP, name, parent, campus, zone, mail } = req.body;
  if (!oldIP || !name || !parent || !campus || !zone)
    return res.status(401).send('Campos vacios');
  const newReport = new Modif_Report({
    _id: new mongoose.Types.ObjectId(),
    user: req.session.user,
    type: 'Edition',
    date: moment().locale('es').format('dddd DD MMMM YYYY, hh:mm:ss A'),
    sw_IP: oldIP,
    data: {
      IP: IP,
      name: name,
      parent: parent,
      campus: campus,
      zone: zone,
      mail: mail ? true : false
    }
  });
  try {
    // Check if new IP is already registered in NMS
    if (IP !== oldIP && (await SW_List.findOne({ IP: IP })))
      throw {
        error: 'La dirección IP ya está registrada'
      };
    await SW_List.findOneAndUpdate(
      { IP: oldIP },
      {
        IP: IP,
        name: name,
        parent: parent,
        campus: campus,
        zone: zone,
        mail: mail ? true : false
      }
    );
    // Change parent IP of child SWs
    const updated = await SW_List.updateMany({ parent: oldIP }, { $set: { parent: IP } });
    if (updated.n > 0)
      for (const sw of Object.values(req.app.get('SWs')))
        if (sw.parent === oldIP) sw.parent = IP
    await newReport.save();
    delete req.app.get('SWs')[oldIP]; // Remove old Switch IP from NMS
    // Add new Switch IP to NMS
    req.app.get('SWs')[IP] = {
      IP: IP,
      name: name,
      parent: parent,
      mail: mail ? true : false,
      notified: false
    };
    res.send('UPD');
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
}

/**
 * Remove a Switch from NMS and database.
 * @param {Request} req
 * @param {Response} res
 */
async function deleteSwitch(req, res) {
  const IP = req.params.IP;
  if (!IP) return res.status(401).send('IP vacia');
  try {
    const SWs = Object.values(req.app.get('SWs'));
    // Verify if given Switch IP is parent from other device
    for (sw of SWs)
      if (sw.parent === IP)
        throw {
          error: 'El switch tiene nodos hijos, no puede eliminarse'
        };
    const delSw = await SW_List.findOne({ IP: IP });
    await SW_List.deleteOne({ IP: IP });
    delete req.app.get('SWs')[IP]; // Remove Switch from NMS
    const newReport = new Modif_Report({
      _id: new mongoose.Types.ObjectId(),
      user: req.session.user,
      type: 'Elimination',
      date: moment().locale('es').format('dddd DD MMMM YYYY, hh:mm:ss A'),
      sw_IP: IP,
      data: {
        name: delSw.name,
        parent: delSw.parent,
        campus: delSw.campus,
        zone: delSw.zone,
        mail: delSw.mail
      }
    });
    await newReport.save();
    res.send('DEL');
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
}

/**
 * Get from given IP the Switch data (name, basic info, VLANs list and Ports data (name, alias, speed, type and VLANs assigned)) and send JSON data to client.
 * @param {Request} req
 * @param {Response} res
 */
async function getSwDataAPI(req, res) {
  // Get Switch data by URL IP address param (url ex: http://10.16.17.254:8000/status/switch/10.16.16.60)
  try {
    res.json(await getSwData(req.params.IP, req.app));
  } catch (swDataError) {
    console.error('get API Error: ', swDataError);
    res.status(500).json(swDataError);
  }
}

/**
 * Create a SNMP request with given IP and retrieve all Switch data (name, basic info, VLANs list and Ports data (name, alias, speed, type and VLANs assigned)).
 * @param {String} IP device IP address
 * @param {Express} APP Server application
 * @returns {data} Object that contains Switch data
 */
async function getSwData(IP, APP) {
  return new Promise(async (resolve, reject) => {
    // Start SNPM session to device (IP, community string, options)
    const session = snmp.createSession(IP, CMSTRING, options);
    session.on('error', (snmpError) => {
      session.close();
      return reject(snmpError);
    });
    let swData = {};
    try {
      swData.basic = await getSwInfo(session);
      const swDbData = await getSwModel(swData.basic.oid);
      if (swDbData !== null) {
        swData.basic.brand = swDbData.brand;
        swData.basic.model = swDbData.model;
        swData.basic.type = swDbData.type;
      }
      swData.VLANs = await getSwVLANs(session, swData.basic.oid);
      swData.ports = await getSwPorts(session, swData.basic.oid);
      await loadPortsVLANs(session, swData.ports, swData.basic.oid);
      // JSON VLANs to String Array
      if (swData.VLANs) {
        const VLANs = [];
        const vlanIDs = Object.keys(swData.VLANs);
        const vlanNames = Object.values(swData.VLANs);
        for (i in vlanIDs) VLANs.push({ id: vlanIDs[i], name: vlanNames[i] });
        swData.VLANs = VLANs;
      }
      // JSON ports to String Array
      if (swData.ports) {
        const portIDs = Object.keys(swData.ports);
        const portValues = Object.values(swData.ports);
        for (i in portIDs) portValues[i].id = portIDs[i];
        swData.ports = portValues;
      }
      saveSwReport(IP, swData, APP);
      resolve(swData);
    } catch (err) {
      swData.error = err;
      reject(swData);
    }
    session.close();
  });
}

/**
 * Save reports for Switch in database.
 * @param {String} IP device IP address
 * @param {data} swData Object that contains Switch data
 * @param {Express} APP Server application
 */
// Save current Backup_Report
async function saveSwReport(IP, swData, APP) {
  const swListData = await SW_List.findOne({ IP: IP });
  if (swListData) {
    // Get BackBone port
    for (port of swData.ports)
      if (port.alias && /backbone/i.test(port.alias)) {
        if (APP.get('SWs')[IP]) APP.get('SWs')[IP].parentPort = port.name;
        swListData.parent_port = port.name;
        swListData.save();
      }
    const update = await Backup_Report.findOneAndUpdate(
      { sw_IP: IP, type: 'current' },
      {
        db_data: swListData,
        basic: swData.basic,
        VLANs: swData.VLANs,
        ports: swData.ports,
        date: moment().locale('es').format('dddd DD MMMM YYYY, hh:mm:ss A')
      }
    );
    if (!update)
      new Backup_Report({
        _id: new mongoose.Types.ObjectId(),
        sw_IP: IP,
        type: 'current',
        db_data: swListData,
        basic: swData.basic,
        VLANs: swData.VLANs,
        ports: swData.ports,
        date: moment().locale('es').format('dddd DD MMMM YYYY, hh:mm:ss A')
      }).save();
  }
}

/**
 * Get Switch name registered in database.
 * @param {String} IP device IP address
 * @returns {mongoose.Query} database result for name query
 */
function getSwDbName(IP) {
  return new Promise(async (resolve, reject) => {
    try {
      resolve(await SW_List.findOne({ IP: IP }, { _id: 0, name: 1 }));
    } catch (err) {
      err.section = 'DB';
      reject(err);
    }
  });
}

/**
 * Convert HEX String into Binary String.
 * @param {String} hex Hexadecimal string value to convert
 * @returns {String} Binary representation
 */
function hex2bin(hex) {
  return parseInt(hex, 16).toString(2).padStart(4, '0');
}

/**
 * Get from SNMP request the basic device information.
 * @param {snmp.Session} session Switch SNMP session
 * @returns {data} Object that contains Switch basic information
 */
function getSwInfo(session) {
  return new Promise((resolve, reject) => {
    // OIDs for Basic SW Info => ['ObjectID', 'UpTime', 'Contact', 'Name', 'Location']
    const OIDs = [
      '1.3.6.1.2.1.1.2.0',
      '1.3.6.1.2.1.1.3.0',
      '1.3.6.1.2.1.1.4.0',
      '1.3.6.1.2.1.1.5.0',
      '1.3.6.1.2.1.1.6.0'
    ];
    const INFO = {};
    session.get(OIDs, (error, results) => {
      if (error) {
        error.section = 'INFO';
        reject(error);
      } else {
        // ObjectID
        const OID = results[0];
        if (snmp.isVarbindError(OID)) console.error(snmp.varbindError(OID));
        else INFO.oid = OID.value.toString();
        // UpTime
        const UPTIME = results[1];
        if (snmp.isVarbindError(UPTIME))
          console.error(snmp.varbindError(UPTIME));
        else INFO.uptime = prettyms(UPTIME.value * 10, { verbose: true }); // item.value => from centiseconds to miliseconds
        // Contact
        const CONTACT = results[2];
        if (snmp.isVarbindError(CONTACT))
          console.error(snmp.varbindError(CONTACT));
        else INFO.contact = CONTACT.value.toString();
        // Name
        const NAME = results[3];
        if (snmp.isVarbindError(NAME)) console.error(snmp.varbindError(NAME));
        else INFO.name = NAME.value.toString();
        // Location
        const LOCATION = results[4];
        if (snmp.isVarbindError(LOCATION))
          console.error(snmp.varbindError(LOCATION));
        else INFO.location = LOCATION.value.toString();
        // SNMP query for get Switch serial
        session.subtree(
          '1.3.6.1.2.1.47.1.1.1.1.11',
          (results) => {
            if (INFO.serial) return; // Patch for already serial filled
            for (const item of results) {
              if (snmp.isVarbindError(item))
                console.error('SERIAL Error: ', snmp.varbindError(item));
              else if (item.value.toString() === '') continue;
              else INFO.serial = item.value.toString().trim();
              break;
            }
          },
          (error) => {
            if (error) {
              error.section = 'SERIAL';
              reject(error);
            } else resolve(INFO);
          }
        );
      }
    });
  });
}

/**
 * Get Switch model name from supported models list in database.
 * @param {String} SW_OID Switch OID
 * @returns {mongoose.Query} database result for model query
 */
function getSwModel(SW_OID) {
  return new Promise(async (resolve, reject) => {
    try {
      resolve(await SW_Model.findOne({ oid: SW_OID }));
    } catch (err) {
      reject(err);
    }
  });
}

/**
 * Get Switch OID value.
 * @param {snmp.Session} session Switch SNMP session
 * @returns {String} Switch OID value
 */
function getSwOID(session) {
  return new Promise((resolve, reject) => {
    const OID = ['1.3.6.1.2.1.1.2.0'];
    session.get(OID, (error, results) => {
      if (error) {
        error.section = 'OID';
        reject(error);
      } else {
        // ObjectID
        const OID = results[0];
        if (snmp.isVarbindError(OID)) console.error(snmp.varbindError(OID));
        else resolve(OID.value.toString());
      }
    });
  });
}

/**
 * Get Switch ports data.
 * @param {snmp.Session} session Switch SNMP session
 * @param {String} SW_OID Switch OID
 * @returns {data} Object that contains Switch ports data
 */
function getSwPorts(session, SW_OID) {
  return new Promise((resolve, reject) => {
    let maxRep = 2;
    let OID = '1.3.6.1.2.1.2.2.1.8';
    let ciscoSG = false; // For the break in SGXXX models
    let portTypeOID;
    const PORTS = {};
    switch (SW_OID) {
      // Catalyst Switches
      case '1.3.6.1.4.1.9.1.1745':
      case '1.3.6.1.4.1.9.1.1208':
        maxRep = 1;
        OID = '1.3.6.1.4.1.9.9.46.1.6.1.1.2';
        portTypeOID = '1.3.6.1.4.1.9.9.46.1.6.1.1.13';
        break;
      // Cisco Switches
      case '1.3.6.1.4.1.9.6.1.94.48.5':
      case '1.3.6.1.4.1.9.6.1.94.48.1':
      case '1.3.6.1.4.1.9.6.1.91.24.8':
      case '1.3.6.1.4.1.9.6.1.81.28.2':
      case '1.3.6.1.4.1.9.6.1.85.48.1':
        maxRep = 20;
        portTypeOID = '1.3.6.1.4.1.9.6.1.101.48.22.1.1';
        ciscoSG = true;
        break;
      // let maxRep = 2 // for HP v1910 ***
    }
    session.subtree(
      OID,
      maxRep,
      (results) => {
        for (const item of results) {
          if (snmp.isVarbindError(item))
            console.error('PORT Error: ', snmp.varbindError(item));
          else {
            const SPLIT = item.oid.split('.');
            const PORT = SPLIT[SPLIT.length - 1]; // Last number in OID is Port Index
            if (ciscoSG) {
              if (PORT.length > 3) break; // Port Index never past over 3 digits; over 3 digits are other type of interfaces
              if (item.value === 6) continue; // Not present interface
            }
            PORTS[PORT] = {};
            // OIDs for port info => [ Port Descp (name), Port Descp (Alias), Port Status (code number), Port Type (code number)... ]
            let OIDs = [
              `1.3.6.1.2.1.2.2.1.2.${PORT}`,
              `1.3.6.1.2.1.31.1.1.1.18.${PORT}`,
              `1.3.6.1.2.1.31.1.1.1.15.${PORT}`
            ];
            if (ciscoSG) PORTS[PORT].status = item.value;
            else OIDs.push(`1.3.6.1.2.1.2.2.1.8.${PORT}`);
            if (portTypeOID) OIDs.push(`${portTypeOID}.${PORT}`);
            session.get(OIDs, (error, data) => {
              if (error) console.error('PORT Data Error: ', error.toString());
              else {
                // Name
                let item = data[0];
                if (snmp.isVarbindError(item))
                  console.error(snmp.varbindError(item));
                // Verify if it's a valid Ethernet Interface port
                else if (
                  item.value.toString().toUpperCase().includes('ETHERNET')
                )
                  PORTS[PORT].name = item.value.toString();
                else {
                  delete PORTS[PORT];
                  return;
                }
                // Alias
                item = data[1];
                if (snmp.isVarbindError(item))
                  console.error(snmp.varbindError(item));
                else PORTS[PORT].alias = item.value.toString();
                // Speed
                item = data[2];
                if (snmp.isVarbindError(item))
                  console.error(snmp.varbindError(item));
                else PORTS[PORT].speed = item.value;
                // Status
                if (!ciscoSG) {
                  item = data[3];
                  if (snmp.isVarbindError(item))
                    console.error(snmp.varbindError(item));
                  else if (item.value !== 6) PORTS[PORT].status = item.value;
                  else {
                    delete PORTS[PORT];
                    return;
                  }
                }
                // Type
                if (portTypeOID) {
                  if (ciscoSG) item = data[3];
                  else item = data[4];
                  if (snmp.isVarbindError(item))
                    console.error(snmp.varbindError(item));
                  else PORTS[PORT].type = item.value;
                }
                // Create VLANs array per port
                PORTS[PORT].VLANs = [];
              }
            });
          }
        }
      },
      (error) => {
        if (error) {
          error.section = 'PORTS';
          reject(error);
        } else resolve(PORTS);
      }
    );
  });
}

/**
 * Get Switch VLANs list.
 * @param {snmp.Session} session Switch SNMP session
 * @param {String} SW_OID Switch OID
 * @returns {data} Object that contains Switch VLANs list
 */
function getSwVLANs(session, SW_OID) {
  return new Promise((resolve, reject) => {
    let OID;
    const VLANs = {};
    switch (SW_OID) {
      // Catalyst Switches
      case '1.3.6.1.4.1.9.1.1745':
      case '1.3.6.1.4.1.9.1.1208':
        // OID for vtpVlanName
        OID = '1.3.6.1.4.1.9.9.46.1.3.1.1.4';
        break;
      default:
        // OID for VLANs Static-Table
        OID = '1.3.6.1.2.1.17.7.1.4.3.1.1';
        break;
    }
    session.subtree(
      OID,
      (results) => {
        for (const item of results) {
          if (snmp.isVarbindError(item))
            console.error('VLAN Error: ', snmp.varbindError(item));
          else {
            const split = item.oid.split('.');
            const vlan = split[split.length - 1]; // Last number in OID is VLAN ID
            let name;
            // Check empty descriptions
            if (item.value.toString() === '') name = 'VLAN ' + vlan;
            else name = item.value.toString();
            VLANs[vlan] = name;
          }
        }
      },
      (error) => {
        if (error) {
          error.section = 'VLANS';
          reject(error);
        } else resolve(VLANs);
      }
    );
  });
}

/**
 * Assign linked VLANs into each port.
 * @param {snmp.Session} session Switch SNMP session
 * @param {[PORT]} PORTS Switch ports list
 * @param {String} SW_OID Switch OID
 */
function loadPortsVLANs(session, PORTS, SW_OID) {
  return new Promise(async (resolve, reject) => {
    if (!PORTS) reject({ section: 'VLANS PORTS' });
    let OID;
    switch (SW_OID) {
      // Catalyst Switches
      case '1.3.6.1.4.1.9.1.1208':
      case '1.3.6.1.4.1.9.1.1745':
        try {
          for (let i = 0; i < 4; i++) {
            // Trunk ports - sending VLAN group
            await loadTrunkVlansCatalyst(session, PORTS, i);
            // Access ports
            resolve(await loadAccessVlansCatalyst(session, PORTS));
          }
        } catch (err) {
          reject(err);
        }
        return;
      // Cisco Switches
      case '1.3.6.1.4.1.9.6.1.94.48.5':
      case '1.3.6.1.4.1.9.6.1.94.48.1':
      case '1.3.6.1.4.1.9.6.1.91.24.8':
      case '1.3.6.1.4.1.9.6.1.81.28.2':
      case '1.3.6.1.4.1.9.6.1.85.48.1':
        // OID for Current VLAN EgressPorts
        OID = '1.3.6.1.2.1.17.7.1.4.2.1.4.0';
        break;
      default:
        // OID for Static VLAN EgressPorts
        OID = '1.3.6.1.2.1.17.7.1.4.3.1.2';
        break;
    }
    session.subtree(
      OID,
      (results) => {
        for (const item of results) {
          if (snmp.isVarbindError(item))
            console.error('VLANS PORT Error: ', snmp.varbindError(item));
          else {
            const split = item.oid.split('.');
            const vlan = split[split.length - 1]; // Last number in OID is VLAN ID
            const HEX = item.value.toString('hex');
            let binary = '';
            let vlanPorts = [];
            for (const i of HEX) binary += hex2bin(i);
            for (let i = 0; i < binary.length; i++)
              if (binary.charAt(i) === '1') vlanPorts.push(i + 1);
            for (const port of vlanPorts)
              if (PORTS[port] !== undefined) PORTS[port].VLANs.push(vlan);
          }
        }
      },
      (error) => {
        if (error) {
          error.section = 'VLANS PORT';
          reject(error);
        } else resolve();
      }
    );
  });
}

/**
 * Assign linked VLANs into Access ports.
 * @param {snmp.Session} session Switch SNMP session
 * @param {[PORT]} PORTS Switch ports list
 */
function loadAccessVlansCatalyst(session, PORTS) {
  // OID for vmMembershipEntry (default vlan)
  const OID = '1.3.6.1.4.1.9.9.68.1.2.2.1.2';
  return new Promise((resolve, reject) => {
    session.subtree(
      OID,
      (results) => {
        for (const item of results) {
          if (snmp.isVarbindError(item))
            console.error('ACCESS VLAN Error: ', snmp.varbindError(item));
          else {
            const split = item.oid.split('.');
            const port = split[split.length - 1]; // Last number in OID is port ID
            const vlan = item.value.toString();
            if (!(item.value in PORTS[port].VLANs))
              PORTS[port].VLANs.push(vlan);
          }
        }
      },
      (error) => {
        if (error) {
          error.section = 'ACCESS VLANS';
          reject(error);
        } else resolve();
      }
    );
  });
}

/**
 * Assign linked VLANs into Trunk ports.
 * @param {snmp.Session} session Switch SNMP session
 * @param {[PORT]} PORTS Switch ports list
 * @param {Number} loop Current VLAN group loop. (Each group has a size of 1024)
 */
function loadTrunkVlansCatalyst(session, PORTS, loop) {
  // OID for vlanTrunkPortVlansEnabled (k,2k,3k,4k)
  const OIDs = [
    '1.3.6.1.4.1.9.9.46.1.6.1.1.4',
    '1.3.6.1.4.1.9.9.46.1.6.1.1.17',
    '1.3.6.1.4.1.9.9.46.1.6.1.1.18',
    '1.3.6.1.4.1.9.9.46.1.6.1.1.19'
  ];
  // Default SNMP VLANs values for NO VLAN assigned
  const badHEXs = [
    'fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe',
    'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',
    '7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',
    ''
  ];
  return new Promise((resolve, reject) => {
    session.subtree(
      OIDs[loop],
      (results) => {
        for (const item of results) {
          if (snmp.isVarbindError(item))
            console.error('TRUNK VLAN Error: ', snmp.varbindError(item));
          else {
            const split = item.oid.split('.');
            const port = split[split.length - 1]; // Last number in OID is port ID
            const HEX = item.value.toString('hex');
            if (!badHEXs.includes(HEX)) {
              let binary = '';
              let vlansPort = [];
              for (const i of HEX) binary += hex2bin(i);
              for (let i = 0; i < binary.length; i++)
                if (binary.charAt(i) === '1')
                  vlansPort.push((i + 1024 * loop).toString());
              for (const vlan of vlansPort)
                if (PORTS[port] !== undefined) PORTS[port].VLANs.push(vlan);
            }
          }
        }
      },
      (error) => {
        if (error) {
          error.section = 'TRUNK VLANS';
          reject(error);
        } else resolve();
      }
    );
  });
}

module.exports.getSwitch = getSwitch;
module.exports.addSwitch = addSwitch;
module.exports.editSwitch = editSwitch;
module.exports.deleteSwitch = deleteSwitch;
module.exports.getSwData = getSwData;
module.exports.getSwDataAPI = getSwDataAPI;
module.exports.getSwDbName = getSwDbName;
module.exports.getSwOID = getSwOID;
module.exports.getSwPorts = getSwPorts;
