/**
 * Controller for data management of devices status monitor route
 * @module controllers:monitorController
 */

/** Return devices status in the NMS. */
module.exports.getSWs = (req, res) => res.json(req.app.get('SWs'));
