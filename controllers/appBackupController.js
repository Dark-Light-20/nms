/**
 * Controller for application backup processes.
 * @module controllers:appBackupController
 */

// DB Models
const User = require('../models/User');
const SW_Model = require('../models/SW_Model');
const SW_List = require('../models/SW_List');
const Modif_Report = require('../models/Modif_Report');
const Backup_Report = require('../models/Backup_Report');
const Status_Report = require('../models/Status_Report');

/**
 * Retrieve all DB and Switches loaded in the NMS for write them into a JSON backup file and send it to the client.
 * @param {Request} req
 * @param {Response} res
 */
async function getAppBackup(req, res) {
  try {
    const BACKUP = {};
    // Retrieve DB models into BACKUP
    BACKUP.User = await User.find({});
    BACKUP.SW_Model = await SW_Model.find({});
    BACKUP.SW_List = await SW_List.find({});
    BACKUP.Status_Report = await Status_Report.find({});
    BACKUP.Modif_Report = await Modif_Report.find({});
    BACKUP.Backup_Report = await Backup_Report.find({});
    // Retrieve switches loaded in NMS into BACKUP
    BACKUP.SWs = req.app.get('SWs');
    // Create backup file and sent it to client
    res.attachment('NMSbackup.json');
    res.json(BACKUP);
  } catch (err) {
    console.error('Backup error: ' + err);
    res.status(500).send(err);
  }
}

/**
 * Load the data from the backup sent by the client and replace it into the NMS.
 * @param {Request} req
 * @param {Response} res
 */
async function loadAppBackup(req, res) {
  // Retrieve backup file from client
  if (req.file && req.file.mimetype === 'application/json') {
    try {
      // Parse backup file data
      const data = JSON.parse(req.file.buffer.toString());
      // Load backup data
      if (data.User) {
        await User.deleteMany({});
        for (user of data.User) await new User(user).save();
      }
      if (data.SW_Model) {
        await SW_Model.deleteMany({});
        for (swModel of data.SW_Model) await new SW_Model(swModel).save();
      }
      if (data.SW_List) {
        await SW_List.deleteMany({});
        for (swList of data.SW_List) await new SW_List(swList).save();
      }
      if (data.Status_Report) {
        await Status_Report.deleteMany({});
        for (status of data.Status_Report) await new Status_Report(status).save();
      }
      if (data.Modif_Report) {
        await Modif_Report.deleteMany({});
        for (modif of data.Modif_Report) await new Modif_Report(modif).save();
      }
      if (data.Backup_Report) {
        await Backup_Report.deleteMany({});
        for (backup of data.Backup_Report) await new Backup_Report(backup).save();
      }
      if (data.SWs) req.app.set('SWs', data.SWs);
      console.log('Backup restored');
      res.send('Backup file restored');
    } catch (err) {
      console.error('Load backup error: ' + err)
      res.status(500).send(err)
    }
  }
  else {
    console.error('Backup File Error');
    res.status(500).send('Archivo de backup incorrecto');
  }
}

module.exports.getAppBackup = getAppBackup;
module.exports.loadAppBackup = loadAppBackup;
