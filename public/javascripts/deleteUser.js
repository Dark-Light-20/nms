$('#deleteUserForm').on('submit', (e) => {
  e.preventDefault();
  $('#btnDelete').attr('disabled', true);
  const deleteData = new FormData($('#deleteUserForm')[0]);
  $.ajax({
    type: 'POST',
    url: '/deleteUser',
    processData: false,
    contentType: false,
    data: deleteData,
    success: () => {
      $('#msgDelete').show();
      $('#msgDelete').removeClass('alert-danger');
      $('#msgDelete').addClass('alert-success');
      $('#msgDelete').text('Usuario eliminado');
    },
    error: (jqXHR) => {
      const errorMessage = `Error (cod: ${jqXHR.status}) : ${jqXHR.responseText}`;
      console.error(errorMessage);
      $('#msgDelete').show();
      $('#msgDelete').removeClass('alert-success');
      $('#msgDelete').addClass('alert-danger');
      $('#msgDelete').text(errorMessage);
    }
  }).always(() => $('#btnDelete').attr('disabled', false));
});
