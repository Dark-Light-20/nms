$('.nav-link').removeClass('active');
$($('.nav-link')[2]).addClass('active');
// Get SW IP
const IP = $('#IP').text();
// Match table sizes
const infoTable = $('#infoTable');
const vlanTable = $('#vlanTable');
vlanTable.height(infoTable.height());
// change view Buttons
$('#btnTableView').on('click', () => {
  $('#btnCardsView').removeClass('btn-success');
  $('#btnCardsView').addClass('btn-secondary');
  $('#btnTableView').removeClass('btn-secondary');
  $('#btnTableView').addClass('btn-success');
  $('.portCard').hide(500, () => $('.portsTable').show(500));
});
$('#btnCardsView').on('click', () => {
  $('#btnTableView').removeClass('btn-success');
  $('#btnTableView').addClass('btn-secondary');
  $('#btnCardsView').removeClass('btn-secondary');
  $('#btnCardsView').addClass('btn-success');
  $('.portsTable').hide(500, () => $('.portCard').show(500));
});
// Define vars for charts
var width = 200;
var height = 200;
var currentX = 0;
var duration = 2000;
var step = 15;
// Create charts
// Add row div in active ports
const activePorts = $('.bg-success');
activePorts.siblings('.card-body').append('<div class="row row-chart"></div>');
// Add SVG for out traffic chart
$('<div class="col col-out"></div>').appendTo('.row-chart');
$('<h4 class="text-primary">Tráfico de salida:</h4>').appendTo('.col-out');
$('<svg class="chart-out"></svg>').appendTo('.col-out');
// Add SVG for in traffic chart
$('<div class="col col-in"></div>').appendTo('.row-chart');
$('<h4 class="text-danger">Tráfico de entrada:</h4>').appendTo('.col-in');
$('<svg class="chart-in"></svg>').appendTo('.col-in');
// D3 char variables
var chartOut = d3
  .selectAll('.chart-out')
  .attr('width', width)
  .attr('height', height);
var chartIn = d3
  .selectAll('.chart-in')
  .attr('width', width)
  .attr('height', height);
var x = d3.scaleLinear().domain([0, 200]).range([0, 200]);
var y = d3.scaleLinear().domain([0, 100]).range([190, 0]);
// Define lines
var line = d3
  .line()
  .x(function (d) {
    return x(d.x);
  })
  .y(function (d) {
    return y(d.y);
  });
var smoothLine = d3
  .line()
  .curve(d3.curveCardinal)
  .x(function (d) {
    return x(d.x);
  })
  .y(function (d) {
    return y(d.y);
  });
var lineArea = d3
  .area()
  .x(function (d) {
    return x(d.x);
  })
  .y0(y(0))
  .y1(function (d) {
    return y(d.y);
  })
  .curve(d3.curveCardinal);
// Append the holder for line chart and fill area
chartOut.append('path').attr('class', 'smoothline-out'); // Line
chartOut.append('path').attr('class', 'area-out'); // Area
chartIn.append('path').attr('class', 'smoothline-in'); // Line
chartIn.append('path').attr('class', 'area-in'); // Area
// Append the legend
const gOut = chartOut.append('g').style('overflow', 'hiden');
const gIn = chartIn.append('g').style('overflow', 'hiden');
// Background rect
gOut
  .append('rect')
  .attr('x', 12)
  .attr('y', 12)
  .attr('rx', 10)
  .attr('ry', 10)
  .attr('width', '100px')
  .attr('height', '30px')
  .style('fill', 'rgba(221, 221, 221, 0.7)');
gIn
  .append('rect')
  .attr('x', 12)
  .attr('y', 12)
  .attr('rx', 10)
  .attr('ry', 10)
  .attr('width', '100px')
  .attr('height', '30px')
  .style('fill', 'rgba(221, 221, 221, 0.7)');
// Percentage text
gOut
  .append('text')
  .attr('x', 20)
  .attr('y', 35)
  .style('fill', '#0f13f1')
  .style('font-size', '150%')
  .text('...%');
gIn
  .append('text')
  .attr('x', 20)
  .attr('y', 35)
  .style('fill', '#e74c3c')
  .style('font-size', '150%')
  .text('...%');
// Init usage Array variables
const usageOutArray = {};
const usageInArray = {};
for (port of activePorts) {
  usageOutArray[$(port).parent().attr('id').substr(8)] = [];
  usageInArray[$(port).parent().attr('id').substr(8)] = [];
}
// Update loop
if (activePorts.length > 0) {
  const interval = setInterval(() => {
    // Update data
    $.ajax({
      type: 'GET',
      url: `/status/traffic/${IP}`,
      success: (usageData) => {
        // Increase step
        currentX += step;
        // Update each value of ports usage array
        for (traffic of usageData) {
          // Add new point to plot
          usageOutArray[traffic.id].push({
            x: currentX,
            y: parseFloat(traffic.usageOut)
          });
          usageInArray[traffic.id].push({
            x: currentX,
            y: parseFloat(traffic.usageIn)
          });
          // Get corresponding chart
          const portChartOut = $(`#cardPort${traffic.id}`)
            .children('.card-body')
            .children('.row-chart')
            .children('.col-out')
            .children('svg');
          const portChartIn = $(`#cardPort${traffic.id}`)
            .children('.card-body')
            .children('.row-chart')
            .children('.col-in')
            .children('svg');
          // Update legend value
          portChartOut
            .children('g')
            .children('text')
            .text(`${traffic.usageOut}%`);
          portChartIn
            .children('g')
            .children('text')
            .text(`${traffic.usageIn}%`);
          // Draw new line
          d3.select(portChartOut.children('.smoothline-out')[0])
            .datum(usageOutArray[traffic.id])
            .attr('d', smoothLine);
          d3.select(portChartIn.children('.smoothline-in')[0])
            .datum(usageInArray[traffic.id])
            .attr('d', smoothLine);
          // Draw new fill area
          d3.select(portChartOut.children('.area-out')[0])
            .datum(usageOutArray[traffic.id])
            .attr('d', lineArea);
          d3.select(portChartIn.children('.area-in')[0])
            .datum(usageInArray[traffic.id])
            .attr('d', lineArea);
          // Shift the chart left
          x.domain([currentX - (width - step), currentX]);
          d3.select(portChartOut.children('.smoothline-out')[0])
            .attr('transform', null)
            .transition()
            .duration(duration)
            .ease(d3.easeLinear, 2)
            .attr('transform', 'translate(' + x(currentX - width) + ')');
          d3.select(portChartIn.children('.smoothline-in')[0])
            .attr('transform', null)
            .transition()
            .duration(duration)
            .ease(d3.easeLinear, 2)
            .attr('transform', 'translate(' + x(currentX - width) + ')');
          d3.select(portChartOut.children('.area-out')[0])
            .attr('transform', null)
            .transition()
            .duration(duration)
            .ease(d3.easeLinear, 2)
            .attr('transform', 'translate(' + x(currentX - width) + ')');
          d3.select(portChartIn.children('.area-in')[0])
            .attr('transform', null)
            .transition()
            .duration(duration)
            .ease(d3.easeLinear, 2)
            .attr('transform', 'translate(' + x(currentX - width) + ')');
          // Remote old data
          if (usageOutArray[traffic.id] > step)
            usageOutArray[traffic.id].shift();
          if (usageInArray[traffic.id] > step) usageInArray[traffic.id].shift();
        }
      },
      error: (jqXHR) => {
        const errorMessage = `${jqXHR.status} : ${jqXHR.statusText} (${jqXHR.responseText})`;
        console.error(errorMessage);
      }
    });
  }, 6500);
}
