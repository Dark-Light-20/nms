$('.nav-link').removeClass('active');
$($('.nav-link')[3]).addClass('active');
// Match table sizes
$('.btn-block').on('click', (e) => {
  setTimeout(() => {
    const backupAccordion = $(`#collapse${e.target.id.substr(3)}`); // e.target.id = btnX
    const infoTable = backupAccordion.find('.infoTable');
    const vlansTable = backupAccordion.find('.vlansTable');
    vlansTable.height(infoTable.height());
  }, 500);
});
