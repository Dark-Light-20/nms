$('#registerForm').on('submit', (e) => {
  e.preventDefault();
  if ($('#pswd1').val() !== $('#pswd2').val()) {
    $('#msgRegister').show();
    $('#msgRegister').removeClass('alert-success');
    $('#msgRegister').addClass('alert-danger');
    $('#msgRegister').text('Las contraseñas no coinciden');
    return;
  }
  $('#btnSignUp').attr('disabled', true);
  const registerData = new FormData($('#registerForm')[0]);
  $.ajax({
    type: 'POST',
    url: '/register',
    processData: false,
    contentType: false,
    data: registerData,
    success: () => {
      $('#msgRegister').show();
      $('#msgRegister').removeClass('alert-danger');
      $('#msgRegister').addClass('alert-success');
      $('#msgRegister').text('Usuario creado correctamente');
    },
    error: (jqXHR) => {
      const errorMessage = `Error (cod: ${jqXHR.status}) : ${jqXHR.responseText}`;
      console.error(errorMessage);
      $('#msgRegister').show();
      $('#msgRegister').removeClass('alert-success');
      $('#msgRegister').addClass('alert-danger');
      $('#msgRegister').text(errorMessage);
    }
  }).always(() => $('#btnSignUp').attr('disabled', false));
});
