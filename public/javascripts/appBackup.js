// Change input file label to file name
$('#backupFile').on('change', (e) =>
    $('#uploadLabel').text(e.target.files[0].name)
);
// Upload backup
$('#appBackupForm').on('submit', (e) => {
    e.preventDefault();
    $('#uploadBtn').attr('disabled', true);
    const data = new FormData();
    data.append('NMSbackup', $('#backupFile')[0].files[0]);
    $.ajax({
        type: "POST",
        url: "/appBackup",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
    })
        .done((response, status) => {
            console.log(status, response);
            $('#msgBackup').removeClass('alert-danger');
            $('#msgBackup').addClass('alert-success');
            $('#msgBackup').text('Se ha restaurado la configuración del sistema');
        })
        .fail((jqXHR) => {
            const errorMessage = `${jqXHR.status} : ${jqXHR.statusText} (${jqXHR.responseText})`;
            console.error(jqXHR);
            $('#msgBackup').removeClass('alert-success');
            $('#msgBackup').addClass('alert-danger');
            $('#msgBackup').text(errorMessage);
        })
        .always(() => {
            $('#msgBackup').show();
            $('#uploadBtn').attr('disabled', false);
        });
})