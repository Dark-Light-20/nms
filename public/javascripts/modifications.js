const table = $('#dataTable').DataTable({
  language: {
    url: '/DataTables/Spanish.json'
  },
  bSort: false
});
$('.nav-link').removeClass('active');
$($('.nav-link')[3]).addClass('active');
// Load Buttons
$('.btnInfo').on('click', (e) => {
  const id = $(e.target).closest('td').attr('id');
  $.ajax({
    type: 'GET',
    url: `/reports/modifications/${id}`
  })
    .done((data) => {
      data = data.data;
      $('#infoName').text(data.name);
      $('#infoParent').text(data.parent);
      $('#infoCampus').text(data.campus);
      $('#infoZone').text(data.zone);
      $('#infoMail').text(data.mail ? 'Si' : 'No');
      $('#infoBody').removeClass('d-none');
    })
    .fail((jqXHR) => {
      const errorMessage = `${jqXHR.status} : ${jqXHR.statusText} (${jqXHR.responseText})`;
      console.error(errorMessage);
      $('#msgInfo').show();
      $('#msgInfo').text(errorMessage);
    })
    .always(() => $('.spinner-border').addClass('d-none'));
});
// Init modals
$('#modalInfo').on('hide.bs.modal', () => {
  setTimeout(() => {
    $('.spinner-border').removeClass('d-none');
    $('#infoBody').addClass('d-none');
    $('#infoName').text('');
    $('#infoParent').text('');
    $('#infoCampus').text('');
    $('#infoZone').text('');
    $('#msgInfo').hide();
  }, 1000);
});
