$('#modifyUserForm').on('submit', (e) => {
  e.preventDefault();
  if ($('#pswd1').val() !== $('#pswd2').val()) {
    $('#msgModify').show();
    $('#msgModify').removeClass('alert-success');
    $('#msgModify').addClass('alert-danger');
    $('#msgModify').text('Las contraseñas no coinciden');
    return;
  }
  $('#btnModify').attr('disabled', true);
  const modifyData = new FormData($('#modifyUserForm')[0]);
  $.ajax({
    type: 'POST',
    url: '/user',
    processData: false,
    contentType: false,
    data: modifyData,
    success: () => {
      $('#msgModify').show();
      $('#msgModify').removeClass('alert-danger');
      $('#msgModify').addClass('alert-success');
      $('#msgModify').text('Contraseña modificada');
    },
    error: (jqXHR) => {
      const errorMessage = `Error (cod: ${jqXHR.status}) : ${jqXHR.responseText}`;
      console.error(errorMessage);
      $('#msgModify').show();
      $('#msgModify').removeClass('alert-success');
      $('#msgModify').addClass('alert-danger');
      $('#msgModify').text(errorMessage);
    }
  }).always(() => $('#btnModify').attr('disabled', false));
});
