const table = $('#dataTable').DataTable({
  language: {
    url: '/DataTables/Spanish.json'
  },
  bSort: false,
  dom: 'Bfrtip',
  buttons: [
    {
      extend: 'copyHtml5',
      text:
        "<button class='btn btn-info'><i class='material-icons'>content_copy</i></button>",
      titleAttr: 'Copiar',
      exportOptions: { columns: [0, 1, 2, 3] }
    },
    {
      extend: 'excelHtml5',
      text:
        "<button class='btn btn-success'><i class='material-icons'>description</i></button>",
      titleAttr: 'Excel',
      exportOptions: { columns: [0, 1, 2, 3] }
    },
    {
      extend: 'pdfHtml5',
      text:
        "<button class='btn btn-danger'><i class='material-icons'>picture_as_pdf</i></button>",
      titleAttr: 'PDF',
      exportOptions: { columns: [0, 1, 2, 3] }
    }
  ],
  initComplete: () => $('.col-auto').prepend('Exportar listado:')
});
$('.nav-link').removeClass('active');
$($('.nav-link')[3]).addClass('active');
