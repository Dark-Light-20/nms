const table = $('#dataTable').DataTable({
  language: {
    url: '/DataTables/Spanish.json'
  },
  dom: 'Bfrtip',
  buttons: [
    {
      extend: 'copyHtml5',
      text:
        "<button class='btn btn-info'><i class='material-icons'>content_copy</i></button>",
      titleAttr: 'Copiar',
      exportOptions: { columns: [0, 1, 2, 3] }
    },
    {
      extend: 'excelHtml5',
      text:
        "<button class='btn btn-success'><i class='material-icons'>description</i></button>",
      titleAttr: 'Excel',
      exportOptions: { columns: [0, 1, 2, 3] }
    },
    {
      extend: 'pdfHtml5',
      text:
        "<button class='btn btn-danger'><i class='material-icons'>picture_as_pdf</i></button>",
      titleAttr: 'PDF',
      exportOptions: { columns: [0, 1, 2, 3] }
    }
  ],
  initComplete: () => $('.col-auto').prepend('Exportar listado:')
});
$('.nav-link').removeClass('active');
$($('.nav-link')[2]).addClass('active');
let tmpSwRow; // Edit/Delete row var
loadModals();
loadBtns();
// * AJAX *
// POST switch
$('#formAddSw').on('submit', (e) => {
  e.preventDefault();
  const newSwitch = new FormData($('#formAddSw')[0]);
  $('#btnFormAdd').attr('disabled', true);
  $.ajax({
    type: 'POST',
    url: `/switch/${newSwitch.get('IP')}`,
    processData: false,
    contentType: false,
    data: newSwitch
  })
    .done((response, status) => {
      console.log(status, response);
      const newRow = $(
        `<tr><td class="align-middle">${newSwitch.get(
          'name'
        )}</td><td class="align-middle" parent="${newSwitch.get(
          'parent'
        )}">${newSwitch.get('IP')}</td><td class="align-middle">${newSwitch.get(
          'campus'
        )}</td><td class="align-middle">${newSwitch.get('zone')}</td>` +
          `</td><td class="align-middle">${
            newSwitch.get('mail') ? 'Si' : 'No'
          }</td>
          ` +
          `<td class="align-middle"><div class="btn-group"><a class="btn btn-success text-body" href="/switch/${newSwitch.get(
            'IP'
          )}">Consultar <i class="material-icons align-middle">info</i></a>` +
          '<button class="btn btn-warning text-body btnEdit" data-toggle="modal" data-target="#modalEditSw">Editar <i class="material-icons align-middle">edit</i></button>' +
          '<button class="btn btn-danger text-body btnDelete" data-toggle="modal" data-target="#modalDeleteSw">Eliminar <i class="material-icons align-middle">remove_circle</i></button>' +
          '</div></td></tr>'
      );
      $('#msgAdd').removeClass('alert-danger');
      $('#msgAdd').addClass('alert-success');
      $('#msgAdd').text('Se ha agregado el nuevo switch');
      setTimeout(() => {
        table.row.add(newRow).draw();
        loadBtns();
        $('#modalAddSw').modal('hide');
      }, 2000);
    })
    .fail((jqXHR) => {
      let errorMessage = `${jqXHR.status} : ${jqXHR.statusText}`;
      if (jqXHR.responseJSON.code && jqXHR.responseJSON.code === 11000)
        errorMessage = errorMessage.concat('. IP ya está registrada.');
      console.error(jqXHR);
      $('#msgAdd').removeClass('alert-success');
      $('#msgAdd').addClass('alert-danger');
      $('#msgAdd').text(errorMessage);
    })
    .always(() => $('#msgAdd').show());
});
// PATCH switch
$('#formEditSw').on('submit', (e) => {
  e.preventDefault();
  const updateSwitch = new FormData($('#formEditSw')[0]);
  $('#btnFormEdit').attr('disabled', true);
  $.ajax({
    type: 'PATCH',
    url: `/switch/${$(tmpSwRow.children('td')[1]).text()}`,
    processData: false,
    contentType: false,
    data: updateSwitch
  })
    .done((response, status) => {
      console.log(status, response);
      $('#msgEdit').removeClass('alert-danger');
      $('#msgEdit').addClass('alert-success');
      $('#msgEdit').text('Se ha actualizado el switch');
      setTimeout(() => {
        $(tmpSwRow.children('td')[0]).text(updateSwitch.get('name'));
        $(tmpSwRow.children('td')[1])
          .text(updateSwitch.get('IP'))
          .attr('parent', updateSwitch.get('parent'));
        $(tmpSwRow.children('td')[2]).text(updateSwitch.get('campus'));
        $(tmpSwRow.children('td')[3]).text(updateSwitch.get('zone'));
        if (updateSwitch.get('mail')) $(tmpSwRow.children('td')[4]).text('Si');
        else $(tmpSwRow.children('td')[4]).text('No');
        $('#modalEditSw').modal('hide');
      }, 2000);
    })
    .fail((jqXHR) => {
      const errorMessage = `${jqXHR.status} : ${jqXHR.statusText} (${jqXHR.responseText})`;
      console.error(jqXHR);
      $('#msgEdit').removeClass('alert-success');
      $('#msgEdit').addClass('alert-danger');
      $('#msgEdit').text(errorMessage);
    })
    .always(() => $('#msgEdit').show());
});
// DELETE switch
$('#btnDeleteSw').on('click', () => {
  const IP = $($(tmpSwRow.children('td'))[1]).text();
  $('#btnDeleteSw').attr('disabled', true);
  $.ajax({
    type: 'DELETE',
    url: `/switch/${IP}`
  })
    .done((response, status) => {
      console.log(status, response);
      $('#msgDel').removeClass('alert-danger');
      $('#msgDel').addClass('alert-info');
      $('#msgDel').text('Se ha eliminado el switch');
      setTimeout(() => {
        table.row(tmpSwRow).remove().draw();
        $('#modalDeleteSw').modal('hide');
      }, 2000);
    })
    .fail((jqXHR) => {
      const errorMessage = `${jqXHR.status} : ${jqXHR.statusText} (${jqXHR.responseText})`;
      console.error(jqXHR);
      $('#msgDel').removeClass('alert-info');
      $('#msgDel').addClass('alert-warning');
      $('#msgDel').text(errorMessage);
    })
    .always(() => $('#msgDel').show());
});

function loadModals() {
  // Init modals fields
  $('#modalAddSw').on('hide.bs.modal', () => {
    $('#formAddSw')[0].reset();
    $('#msgAdd').hide();
    $('#btnFormAdd').attr('disabled', false);
  });
  $('#modalEditSw').on('hide.bs.modal', () => {
    $('#formEditSw')[0].reset();
    $('#msgEdit').hide();
    $('#btnFormEdit').attr('disabled', false);
  });
  $('#modalDeleteSw').on('hide.bs.modal', () => {
    $('#msgDel').hide();
    $('#btnDeleteSw').attr('disabled', false);
  });
}

function loadBtns() {
  // Init buttons
  $('.btn-success').on('click', () => {
    $('.container').html('<div class="spinner-border loader"></div>');
  });
  $('.btnEdit').on('click', (e) => {
    tmpSwRow = $(e.target).closest('tr');
    $('#nameEdit').val($(tmpSwRow.children('td')[0]).text());
    $('#IPedit').val($(tmpSwRow.children('td')[1]).text());
    $('#parentEdit').val($(tmpSwRow.children('td')[1]).attr('parent'));
    $('#campusEdit').val($(tmpSwRow.children('td')[2]).text());
    $('#zoneEdit').val($(tmpSwRow.children('td')[3]).text());
    if ($(tmpSwRow.children('td')[4]).text() === 'Si')
      $('#mailEdit').prop('checked', true);
  });
  $('.btnDelete').on('click', (e) => {
    tmpSwRow = $(e.target).closest('tr');
    const name = $(tmpSwRow.children('td')[0]).text();
    const IP = $(tmpSwRow.children('td')[1]).text();
    $('#delSw').text(`${name} (${IP})`);
  });
}
