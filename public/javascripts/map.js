$('.nav-link').removeClass('active');
$($('.nav-link')[1]).addClass('active');
// SVG HTML element with D3.js properties
const svg = d3.select('#map');
// Layout measurement variables
const width = 2500;
const height = 2000;
const margin = { top: 0, right: 20, bottom: 0, left: 20 };
// Load SVG size
const innerWidth = width - margin.left - margin.right;
const innerHeight = height - margin.top - margin.bottom;
svg.attr('width', width).attr('height', height);
// Create g element for tree layout
const g = svg
  .append('g')
  .attr('transform', `translate(${margin.left},${margin.top})`);
// Generate the Tree Map
const treeLayout = d3.tree().size([innerHeight, innerWidth]);
// Function for generate path links
const linkPathGen = d3
  .linkHorizontal()
  .x((d) => d.y)
  .y((d) => d.x);
// Zoom functionality
svg.call(
  d3.zoom().on('zoom', (event) => {
    g.attr('transform', event.transform);
  })
);
// Get SWs from server for node tree
$.ajax({
  type: 'GET',
  url: '/status/monitor',
  success: loadData,
  error: (_jqXHR, _textStatus, error) => console.error(error)
});

function loadData(data) {
  // Switches list
  const SWs = Object.values(data);
  // Proccess Switches list to D3.js hierarchy strcut
  const rootNode = d3
    .stratify()
    .id((sw) => sw.IP)
    .parentId((sw) => sw.parent)(SWs);
  // Generate links between nodes
  const links = treeLayout(rootNode).links();
  // Generate the link path for the Tree
  g.selectAll('path').data(links).enter().append('path').attr('d', linkPathGen);
  // Generate nodes elements
  const node = g
    .selectAll('.node')
    .data(rootNode.descendants())
    .enter()
    .append('g')
    .attr(
      'class',
      (d) => 'node ' + (d.children ? 'node--internal' : 'node-leaf')
    )
    .attr('transform', (d) => `translate(${d.y}, ${d.x})`)
    .attr('SW', (d) => d.data.IP);
  // Add circle elements into nodes
  node.append('circle').attr('r', 10).attr('class', 'pulse');
  node
    .append('circle')
    .attr('r', 8)
    .attr('data-toggle', 'tooltip')
    .attr('data-placement', (d) => (d.children ? 'right' : 'left'))
    .attr('data-html', 'true')
    .attr('title', (d) => `<b>IP:</b> ${d.data.IP}`)
    .on('click', (e, d) => {
      $('svg').addClass('d-none');
      $('.loader').removeClass('d-none');
      window.location.href = `/switch/${d.data.IP}`;
    })
    .on('mouseenter', (e) => $(e.target).css('cursor', 'pointer'))
    .on('mouseleave', (e) => $(e.target).css('cursor', 'auto'));
  // Add Switches names into nodes
  node
    .append('text')
    .attr('dy', (d) => (d.children ? -4 : 4))
    .attr('x', (d) => (d.children ? -10 : 10))
    .style('text-anchor', (d) => (d.children ? 'end' : 'start'))
    .style('font-size', 12)
    .text((d) => d.data.name);
  // Activate tooltips
  $(() => $('[data-toggle="tooltip"]').tooltip());
  // Reload node status color - daemon
  const nodes = $('.node');
  // First update status of devices
  updateStatuses(data, nodes);
  // Remove loader
  $('svg').removeClass('d-none');
  $('.loader').addClass('d-none');
  // Init client monitor daemon to refresh devices status
  setInterval(() => {
    $.ajax({
      type: 'GET',
      url: '/status/monitor',
      success: (data) => updateStatuses(data, nodes),
      error: (_jqXHR, _textStatus, error) => console.error(error)
    });
  }, 10000);
}

function updateStatuses(dataStatus, nodes) {
  listSWsStatus = Object.values(dataStatus);
  for (sw of listSWsStatus) {
    for (nodeSW of nodes) {
      if ($(nodeSW).attr('SW') === sw.IP) {
        // Assing node color by device status
        if (sw.alive !== undefined) {
          sw.alive
            ? $(nodeSW)
                .children('circle')
                .attr('fill', () => (sw.ms > 50 ? 'gold' : 'green'))
                .attr('stroke', () => (sw.ms > 50 ? 'gold' : 'green'))
                .attr(
                  'data-original-title',
                  `<b>IP:</b> ${sw.IP} ${
                    sw.parentPort
                      ? '<br> <b>BackBone Port:</b> ' + sw.parentPort
                      : ''
                  } <br> <b>ms:</b> ${sw.ms}`
                )
            : $(nodeSW)
                .children('circle')
                .attr('fill', 'red')
                .attr('stroke', 'red')
                .attr(
                  'data-original-title',
                  `<b>IP:</b> ${sw.IP} ${
                    sw.parentPort
                      ? '<br> <b>BackBone Port:</b> ' + sw.parentPort
                      : ''
                  }`
                );
          break;
        } else {
          $(nodeSW)
            .children('circle')
            .attr('fill', 'black')
            .attr('stroke', 'black')
            .attr(
              'data-original-title',
              `<b>IP:</b> ${sw.IP} ${
                sw.parentPort
                  ? '<br> <b>BackBone Port:</b> ' + sw.parentPort
                  : ''
              }`
            );
        }
      }
    }
  }
}
