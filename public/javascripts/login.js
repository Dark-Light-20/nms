$('#loginForm').on('submit', (e) => {
  e.preventDefault();
  const loginData = new FormData($('#loginForm')[0]);
  $.ajax({
    type: 'POST',
    url: '/login',
    processData: false,
    contentType: false,
    data: loginData,
    success: () => (window.location.href = '/'),
    error: (jqXHR) => {
      const errorMessage = `Error (cod: ${jqXHR.status}) : ${jqXHR.responseText}`;
      console.error(errorMessage);
      $('#msgLogin').show();
      $('#msgLogin').text(errorMessage);
    }
  });
});
