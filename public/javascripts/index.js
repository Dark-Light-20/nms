$('#searchForm').on('submit', (e) => {
  e.preventDefault();
  const IP = $('#IPSearch')[0];
  if ($(IP).val() !== '' && IP.checkValidity()) {
    $('.loader').removeClass('d-none');
    $('.container').addClass('d-none');
    window.location.href = `/switch/${$('#IPSearch').val()}`
  }
});
